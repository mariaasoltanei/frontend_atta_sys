import './App.css';
import React from "react";
import {Route, Routes} from "react-router-dom";
import {Component} from "react";
import MainPage from "./pages/main_page";
import UserPage from "./pages/user_page";
import SignUpPage from "./pages/sign_up";

class App extends Component {
    render() {
        return (
            <div className="App">
                <Routes>
                    <Route path="/" element={<MainPage/>}/>
                    <Route path="/user_page" element={<UserPage/>}/>
                    <Route path="/sign_up" element={<SignUpPage/>}/>
                </Routes>
            </div>
        );
    }
}

export default App;