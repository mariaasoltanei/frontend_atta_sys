import client from "./client";

export const getFavouriteSpots = () => client.get("/favourites");