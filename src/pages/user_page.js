import React from 'react';
import NavBar from "../components/nav_bar";
import './css/user_page.css';
import Spots from "../components/spots_list";
import Map from "../components/map";

function UserPage() {
    return (
        <div className="div_user_page">
            <NavBar/>
            <div className="div_user_content">
                <Map/>
                <Spots/>
            </div>
        </div>
    );
}
export default UserPage;