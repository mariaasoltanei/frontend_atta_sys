import './css/main_page.css';
import React from 'react';
import loginImage from '../assets/images/login_image.png'
import {useNavigate} from 'react-router-dom';
import logo from '../assets/images/kite_logo.png'
import LoginInput from "../components/login_input";
import {Button} from "@mui/material";

function MainPage() {
    let navigate = useNavigate();
    return (
        <div className="div_login_page">
            <div className="div_login_data">
                <img id="logo" src={logo}/>
                <LoginInput/>
                <p>or</p>
                <Button variant="contained" onClick={() => navigate('/sign_up')}>Sign up</Button>
            </div>

            <div className="div_login_image">
                <img id="login_image" alt="" src={loginImage}/>
            </div>

        </div>
    );
}

export default MainPage;
