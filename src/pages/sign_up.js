import React from 'react';
import './css/sign_up.css';
import loginImage from "../assets/images/login_image.png";
import logo from "../assets/images/kite_logo.png";
import SignUpInput from "../components/signup_input";

function SignUpPage() {
    return (
        <div className="div_signup_page">
            <div className="div_signup_data">
                <img id="logo" src={logo}/>
                <SignUpInput/>
            </div>
            <div className="div_login_image">
                <img id="login_image" alt="" src={loginImage}/>
            </div>
        </div>
    );
}

export default SignUpPage;