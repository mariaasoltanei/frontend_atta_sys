import React, {useEffect} from "react";
import './css/spots_list.css'
import {Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow} from "@mui/material";
import Spot from "./spot";
import {getSpot} from "../api/spot";
import useApi from "../hooks/useApi";


function Spots() {
    const columns = ['Name', 'Country', 'Latitude', 'Longitude', 'Wind Probability', 'When to go'];
    const getSpotsApi = useApi(getSpot);

    useEffect(() => {
        getSpotsApi.request();
    }, []);

    return (
        <div className="div_spots_table">
            <Paper>
                <TableContainer>
                    <Table>
                        <TableHead>
                            <TableRow>
                                {
                                    columns.map(c => (<TableCell align="center">{c}</TableCell>))
                                }
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {
                                getSpotsApi.data?.map((s) => (<TableRow align="center"><Spot
                                    key={s.id}
                                    name={s.name}
                                    country={s.country}
                                    latitude={s.lat}
                                    longitude={s.long}
                                    wind_probability={s.probability}
                                    when_to_go={s.month}
                                /></TableRow>))
                            }
                        </TableBody>
                    </Table>
                </TableContainer>
            </Paper>
        </div>

    );
}

export default Spots;
