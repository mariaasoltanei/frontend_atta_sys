import React from "react";

const Spot = (props) => {
    return (
        <>
            <td>{props.name}</td>
            <td>{props.country}</td>
            <td>{props.latitude}</td>
            <td>{props.longitude}</td>
            <td>{props.wind_probability}</td>
            <td>{props.when_to_go}</td>
        </>
    );
}

export default Spot;