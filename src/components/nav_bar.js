import React from "react";
import './css/nav_bar.css';
import {BsPersonFill} from "react-icons/bs";
import logo from '../assets/images/kite_logo.png'
import {Button} from '@mui/material';
import {useNavigate} from "react-router-dom";

function NavBar() {
    let navigate = useNavigate();
    return (
        <div className="div_navBar">
            <div className="logo">
                <img id='image_logo' alt="" src={logo}/>
            </div>
            <div className="navBar_actions">
                <Button variant="contained">ADD SPOT</Button>
                <BsPersonFill onClick={() => navigate('/')}/>
            </div>
        </div>
    );
}

export default NavBar;