import React, {useEffect, useState} from "react";
import {Button, TextField} from "@mui/material";
import './css/login_input.css'
import {useNavigate} from "react-router-dom";

import apiClient from "../api/client";

function LoginInput() {
    let navigate = useNavigate();
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [errMsg, setErrMsg] = useState('');
    const [success, setSuccess] = useState(false);
    const postUser = {username, password};

    const handleSubmit = () => {
        if (username !== '' && password !== '') {
            setSuccess(true);
            apiClient.post("/login", postUser);//.then(response => (console.log(response.data.username,response.data.password)));
            navigate('/user_page');
        } else {
            setErrMsg('Missing Username or Password');
        }
    }
    useEffect(() => {
        setErrMsg('');
    }, [username, password])

    return (
        <div className="div_login_input">
            <h3>Username</h3>
            <TextField
                label="Username"
                id="username"
                type="text"
                autoComplete="off"
                onChange={(e) => setUsername(e.target.value)}
                value={username}
                required
            />
            <h3>Password</h3>
            <TextField
                label="Password"
                type="password"
                id="password"
                autoComplete="off"
                onChange={(e) => setPassword(e.target.value)}
                value={password}
                required
            />

            <Button className="btn_login" variant="contained" onClick={handleSubmit}>Log in</Button>
            <p className={errMsg ? "errmsg" : "offscreen"} aria-live="assertive">{errMsg}</p>
        </div>
    );
}

export default LoginInput;