import React, {useEffect, useState} from 'react';
import ReactMap, {Marker} from 'react-map-gl';
import spotImg from '../assets/images/spot.png'
import {getSpot} from "../api/spot";
import useApi from "../hooks/useApi";
import './css/map.css'
import Details from "./details";
import {Button} from "@mui/material";

const accessToken = "pk.eyJ1IjoibWFyaWFhc29sdGFuZWkiLCJhIjoiY2wyYzlyNDd4MGxucjNub2FyNnJ3Y3QzMyJ9.BrUFB9Wr9k4Dil5H16K5lQ";

export default function Map() {
    const [viewport, setViewport] = useState({
        longitude: 17.382934,
        latitude: 54.157934,
        width: "100vw",
        height: "100vh",
        zoom: 3
    });

    const getSpotsApi = useApi(getSpot);
    //const getFavouriteSpotsApi = useApi(getFavouriteSpots);
    const [showPopup, setShowPopup] = useState(false);
    const [selectedSpot, setSelectedSpot] = useState([]);
    const [favSpot, setFavSpot] = useState(false);
    const [listFavSpots, setListFavSpots] = useState([]);
    const [spotsList, setSpotsList] = useState([]);


    const favSpotDetails = (spot) => {
        setFavSpot(spot);
    }
    const listFavS = (list) => {
        setListFavSpots(list);
    }
    useEffect(() => {
        getSpotsApi.request();
        setSpotsList(getSpotsApi.data)
    }, [spotsList]);

    return (
        <div className="div_map_details">
            <div className="div_map">
                <Button variant="contained" onClick={() => {
                    spotsList.filter(w => w.probability > 70).map(v => console.log(v))
                }}>Filter</Button>
                <ReactMap
                    initialViewState={viewport}
                    mapStyle="mapbox://styles/mapbox/streets-v9"
                    mapboxAccessToken={accessToken}
                    onViewportChange={viewport => {
                        setViewport(viewport);
                    }}>
                    {
                        getSpotsApi.data?.map(s => (
                            <Marker key={s.id} latitude={s.lat} longitude={s.long}
                                    onClick={() => {
                                        setShowPopup(true);
                                        setSelectedSpot(s);
                                    }}>

                                <img alt="spot image" src={spotImg}/>
                            </Marker>))

                    }
                </ReactMap>
            </div>

            <div className="div_marker_details">
                {
                    //SHOW DIV ON MARKER CLICK
                    showPopup &&
                    <Details selectedSpot={selectedSpot} favSpotDetails={favSpotDetails} listFavSpots={listFavS}/>
                }
            </div>

        </div>
    );

}