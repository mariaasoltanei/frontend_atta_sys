import React, {useEffect, useState} from "react";
import {Button, TextField} from "@mui/material";
import './css/signup_input.css'
import {useNavigate} from "react-router-dom";

import apiClient from "../api/client";

function SignUpInput() {
    let navigate = useNavigate();
    const [name, setName] = useState('');
    const [password, setPassword] = useState('');
    const [email, setEmail] = useState('');
    const [errMsg, setErrMsg] = useState('');
    const postUser = {
        user: {
            name,
            password,
            email
        }
    };
    const handleSubmit = () => {
        if (name != '' && password != '' && email != '') {
            apiClient.post("/user", postUser);//.then(response => (console.log(response.data.user.name, response.data.user.password)));
            navigate('/user_page');
        } else {
            setErrMsg('Missing something');
        }
    }
    useEffect(() => {
        setErrMsg('');
    }, [name, password, email])

    return (
        <div className="div_signup_input">
            <h3>Name</h3>
            <TextField
                label="Name"
                id="name"
                type="text"
                autoComplete="off"
                onChange={(e) => setName(e.target.value)}
                value={name}
                required
            />
            <h3>Email</h3>
            <TextField
                label="Email"
                id="email"
                type="text"
                autoComplete="off"
                onChange={(e) => setEmail(e.target.value)}
                value={email}
                required
            />
            <h3>Password</h3>
            <TextField
                label="Password"
                type="password"
                id="password"
                autoComplete="off"
                onChange={(e) => setPassword(e.target.value)}
                value={password}
                required
            />

            <Button className="btn_signup" variant="contained" onClick={handleSubmit}>Sign up</Button>
            <p className={errMsg ? "errmsg" : "offscreen"} aria-live="assertive">{errMsg}</p>
        </div>
    );
}

export default SignUpInput;