import React, {useState} from "react";
import './css/details.css';
import {Button} from "@mui/material"
import apiClient from "../api/client";

function Details({selectedSpot, favSpotDetails, listFavSpots}) {
    const [idFav, setIdFav] = useState(null);
    const [post, setPost] = useState(false);
    const [deleteSpot, setDeletePost] = useState(false);
    const [isFavourite, setIsFavourite] = useState(false);
    const [spot, setSpot] = useState({
        id: '',
        name: '',
        country: '',
        lat: '',
        long: '',
        probability: '',
        month: '',
        favourite: false
    })
    let postFavouriteSpot = {spot};

    const setData = () => {
        setSpot({
            id: selectedSpot.id,
            name: selectedSpot.name,
            country: selectedSpot.country,
            lat: selectedSpot.lat,
            long: selectedSpot.long,
            probability: selectedSpot.probability,
            month: selectedSpot.month,
            favourite: true
        })

    }
    const removeData = () => {
        setSpot({
            id: '',
            name: '',
            country: '',
            lat: '',
            long: '',
            probability: '',
            month: '',
            favourite: false
        })
    }

    const handleAddFav = () => {
        apiClient.post("/favourites", postFavouriteSpot);//.then(response => (console.log(response.data.id)));
    }
    return (
        <div className="div_details">
            <div className="details_text">
                <h2>{selectedSpot.name}</h2>
                <p>{selectedSpot.country}</p>
                <h3>Latitude</h3>
                <p>{selectedSpot.lat}</p>
                <h3>Longitude</h3>
                <p>{selectedSpot.long}</p>
                <h3>Wind Probability</h3>
                <p>{selectedSpot.probability} %</p>
                <h3>When to go</h3>
                <p>{selectedSpot.month}</p>
            </div>
            <div className="btn_favorites">
                {
                    <Button variant="contained" onClick={() => {
                        setData()
                        setIsFavourite(true);
                        setPost(true);
                        favSpotDetails(true)
                    }
                    }>Add {post ? (handleAddFav()) : null}</Button>
                }
            </div>
        </div>
    );
}

export default Details;
